##################################################################
# Makefile for LaTeX
##################################################################
# Use:
# make
# make clean
# options for ps2pdf: http://pages.cs.wisc.edu/~ghost/doc/AFPL/6.50/Ps2pdf.htm

FILENAME = EuCAP2017_template
TEX:=$(shell ls *.tex)
OTHER = *~ *.aux *.dvi *.toc *.bbl *.blg *.gz *.out *.thm *.ps *.idx *.ilg *.ind

pdflatex: $(FILENAME).tex
	pdflatex --synctex=1 $(FILENAME).tex
	#bibtex $(FILENAME).aux
	pdflatex --synctex=1 $(FILENAME).tex
	pdflatex --synctex=1 $(FILENAME).tex
	rm -f $(OTHER) $(PS)
clean:
	rm -f $(OTHER) $(PS)
